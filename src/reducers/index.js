const initialState = {
  fetching: false,
  items: []
}

function items(state = initialState, action) {
  switch (action.type) {
    case 'REQUEST_ITEMS':
      return Object.assign({}, state, {
        fetching: true
      });
    case 'RECEIVE_ITEMS':
      return Object.assign({}, state, {
        fetching: false,
        items: action.items
      });
    default:
      return state;
  }
}

export default items;