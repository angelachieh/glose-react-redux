import React from 'react';
import { fetchItems } from '../actions/index';
import items from '../reducers';
import Item from './Item';
import {createStore, compose, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';

const store = createStore(items, compose(applyMiddleware(thunk)));

class ItemList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [],
    };

    store.subscribe(() => {
      this.setState({
        items: store.getState().items
      });
    });
  }

  componentWillMount() {
    store.dispatch(fetchItems());
  }

  render() {
    var items ='';
    if (this.state.items) {
      items = this.state.items.map(function(item) {
        return (
          <Item key={item.id} title={item.title} slug={item.slug} coverimage={item.image} authors={item.authors} />
        );
      });
    }
    return (
      <div className="itemList">
        <div className="gridItems">
          {items}
        </div>
      </div>
    );
  }
}

export default ItemList;