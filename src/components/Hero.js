import React from 'react';

class Hero extends React.Component {
  render() {
    return (
      <div className="hero">
        <div className="title">{this.props.title}</div>
      </div>
    );
  }
}

export default Hero;