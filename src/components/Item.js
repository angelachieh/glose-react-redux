import React from 'react';
import Author from './Author';

class Item extends React.Component {
  render() {
    return (
      <div className="item">
        <a href={"https://glose.com/book/"+this.props.slug+"?list=free-books"}>
          <img src={this.props.coverimage} alt={this.props.title} />
          <div className="title">
            {this.props.title}
          </div>
        </a>
        <Author authors={this.props.authors} />
      </div>
    );
  }
}

export default Item;