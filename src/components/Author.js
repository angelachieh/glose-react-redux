import React from 'react';

class Author extends React.Component {
  render() {
    let authors = this.props.authors;
    if (authors.length === 0) {
      return <div></div>;
    }
    return (
      <div className="authorLine">
        by <a className="author" href={"https://glose.com/author/" + authors[0].slug}>{authors[0].name}</a>
        {authors.length > 1 ? ' et al.' : ''}
      </div>
    ); 
  }
}

export default Author;