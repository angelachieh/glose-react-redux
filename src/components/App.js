import React from 'react';
import Hero from './Hero';
import ItemList from './ItemList';

class App extends React.Component {
  render() {
    return (
      <div>
        <Hero title="Free books" />
        <ItemList />
      </div>
    );
  }
}

export default App;
