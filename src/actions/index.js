export function fetchItems() {
  return dispatch => {
    dispatch(requestItems());
    var requestUrl = 'https://api.glose.com/v1/booklists/free-books?_test=angela';
    fetch(requestUrl).then((response)=>{
      return response.json();
    }).then((data)=>{
      dispatch(receiveItems(data.modules[1].books));
    }).catch((err)=>{
      console.log("There has been an error");
    });
  };
}

function requestItems() {
  return { type: 'REQUEST_ITEMS' }
}

function receiveItems(items) {
  return { type: 'RECEIVE_ITEMS', items };
}